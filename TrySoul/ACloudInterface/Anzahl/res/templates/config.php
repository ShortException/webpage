<?php

//Config File: Edit to your liking! :D
//NOTE: THE LINES FOR THE SERVER NAMES, IPS, & PORTS ALL CORRESPOND TO EACHOTHER
//If you don't want to set a port, just leave that specific line empty! Enjoy! <3 :D
return (object) array(
	
    'title' => 'TrySoulDE | BanPanel | Admin Interface',
    'title_link' => './',
	
	'navbarlinks' => array(
        "Home",
        "Link"
    ),
	
	'navbarlinkdestinations' => array(
        "./",
        "#"
    ),
	
	'servernames' => array(
        "BungeeCord",
        "Lobby-01",
		"Lobby-02"
    ),
	
	'serverips' => array(
        "TrySoul.de",
        "TrySoul.de",
		"TrySoul.de"
    ),
	
	'serverports' => array(
        "25577",
        "15000",
		"25561"
    )
	
);

?>