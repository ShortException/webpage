<?php

//Config File: Edit to your liking! :D
//NOTE: THE LINES FOR THE SERVER NAMES, IPS, & PORTS ALL CORRESPOND TO EACHOTHER
//If you don't want to set a port, just leave that specific line empty! Enjoy! <3 :D
return (object) array(
	
    'title' => 'TrySoulDE | Interface',
    'title_link' => './',
	
	'navbarlinks' => array(
        "Home",
        "Link"
    ),
	
	'navbarlinkdestinations' => array(
        "./",
        "#"
    ),
	
	'servernames' => array(
        "Mineplex",
        "Hypixel",
		"TrySoul.DE"
    ),
	
	'serverips' => array(
        "us.mineplex.com",
        "mc.hypixel.net",
		"TrySoul.de"
    ),
	
	'serverports' => array(
        "25565",
        "25565",
		"25577"
    )
	
);

?>