<div class="footer-margin">
	<footer class="page-footer indigo">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h5 class="white-text"><?php echo $configs->title?></h5>
					<p class="grey-text text-lighten-4">Programmiert von RageCode aka. Manuel Kollus</p>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
				© 2017 Copyright Manuel Kollus
			</div>
		</div>
	</footer>
</div>